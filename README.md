## 文件目录

* 手机端
  * 网页医院详情页 app-index.html
  * 网页版咨询 app-chat.html
* 客服端
  * 工作统计 service-dashboard.html
  * 对话 service-chat.html
  * 设置 service-setting.html
* 管理端
  * 工作统计 admin-dashboard.html
  * 客服 service-list.html
  * 客服详情 service-show.html
  * 新增客服 service-add.html
  * 推广新增医院 hospital-add.html
  * 医院统计 hospital-dashboard.html

## 任务

- [x] 更改配色
- [x] 更改数据图表插件，替换为百度
- [ ] 封装图标插件函数[百度已经封装的足够好，不必再封装]
- [x] app-chat 页面缺返回导航
- [x] 设置和退出按钮放在最下面


- [x] 另外点击【昨天】【上周】【上月】的按钮，日期没有跟着变化
- [x] dashboard 页面的5个色块调整
- [x] 曲线图线的颜色也需要和上面文字的颜色一致，这样清晰
- [x] 【其它待定】的那个模块，删除会自动改变间距和排版吗？如果不是的话，需要删除。
- [x] 推广新增医院可以删除输入框
- [x] 咨询按钮始终悬浮在页面最下方
- [x] 对话框页面在当前页面切换
- [x] 对话界面的所有带颜色的都跟选择的主题色一致
- [x] 对话框使用不可拉伸的
- [x] 百度的 js 下载下来本地引用
- [x] 说明所有页面都需要引入的 css 和 js


## 说明

所有页面都需要引入的 CSS

<link href="css/bootstrap.css" rel="stylesheet">
<link href="font-awesome/css/font-awesome.css" rel="stylesheet">

<link href="css/plugins/switchery/switchery.css" rel="stylesheet">
<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

<link href="css/animate.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">

所有页面都需要引入的 JS

<!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>

<!-- Switchery -->
<script src="js/plugins/switchery/switchery.js"></script>

<script src="js/global.js"></script>


* 其他，根据每个页面的特殊效果比如日期选择器，百度曲线图等，分别需要加载各自的 js，css
* 部分页面有特殊效果，新建了 page-xxx.css page-xxx.js 以避免和其他页面冲突
