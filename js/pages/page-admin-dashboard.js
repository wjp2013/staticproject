( function( $ ) {

var page = '#page-hospital-dashboard';

$( function() {
    // 路径配置
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });

     // 使用
    require(
        [
            'echarts',
            'echarts/chart/line' // 使用柱状图就加载bar模块，按需加载
        ],
        function (ec) {
            // 基于准备好的dom，初始化echarts图表
            var myChart = ec.init(document.getElementById('main'));

            option = {
                title : {
                    text: '工作统计曲线图',
                },
                tooltip : {
                    trigger: 'axis'
                },
                legend: {
                    data:['顾客数','有效对话数','消息数']
                },
                xAxis : [
                    {
                        type : 'category',
                        boundaryGap : false,
                        data : ['周一','周二','周三','周四','周五','周六','周日']
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        axisLabel : {
                            formatter: '{value}'
                        }
                    }
                ],
                color: [
                    '#50ADD4',  '#50C7AF',  '#6790E0'
                ],
                series : [
                    {
                        name:'顾客数',
                        type:'line',
                        data:[100, 120, 210, 281, 291, 131, 210],
                        markPoint : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                        markLine : {
                            data : [
                                {type : 'average', name: '平均值'}
                            ]
                        }
                    },
                    {
                        name:'有效对话数',
                        type:'line',
                        data:[102, 291, 101, 120, 123, 219, 200],
                        markPoint : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                        markLine : {
                            data : [
                                {type : 'average', name : '平均值'}
                            ]
                        }
                    },
                    {
                        name:'消息数',
                        type:'line',
                        data:[232, 121, 221, 223, 132, 219, 291],
                        markPoint : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                        markLine : {
                            data : [
                                {type : 'average', name : '平均值'}
                            ]
                        }
                    },
                ]
            };

            // 为echarts对象加载数据
            myChart.setOption(option);
        }
    );

    $('#data_5 .input-daterange').datepicker({
        format: 'mm/dd/yyyy',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        language: "zh-CN"
    });

    $('.set-yesterday').click(function() {
        var $yesterday = (-1).days().fromNow();
        $('#start-date').datepicker('setDate', $yesterday);
    })
    $('.set-last-week').click(function() {
        var $firstDay = Date.today().last().week().monday();
        $('#start-date').datepicker('setDate', $firstDay);
        console.log($firstDay)
        var $lastDay = Date.today().addWeeks(-1).sunday();
        $('#end-date').datepicker('setDate', $lastDay);
        console.log($lastDay)
    })
    $('.set-last-month').click(function() {
        var $firstDay = Date.today().add(-1).months().moveToFirstDayOfMonth();
        $('#start-date').datepicker('setDate', $firstDay);
        console.log($firstDay)
        var $lastDay = Date.today().add(-1).months().moveToLastDayOfMonth();
        $('#end-date').datepicker('setDate', $lastDay);
        console.log($lastDay)
    })
});


})( jQuery );

