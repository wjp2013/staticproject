( function( $ ) {

var page = '#page-app-index';

$( function() {
    $(page + ' .technology-box').each(function( index ) {
      var text_content = $( this ).text();
      var text_length = text_content.length;
      switch (text_length) {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
          $( this ).css( "line-height", "40px" );
          break;
        case 6:
          text = text_content.substring(0, 3) + '<br>' + text_content.substring(3);
          $( this ).css( "line-height", "20px" );
          $( this ).html(text);
          break;
        case 7:
        case 8:
          text = text_content.substring(0, 4) + '<br>' + text_content.substring(4);
          $( this ).css( "line-height", "20px" );
          $( this ).html(text);
          break;
        case 9:
        case 10:
          text = text_content.substring(0, 5) + '<br>' + text_content.substring(5);
          $( this ).css( "line-height", "20px" );
          $( this ).html(text);
          break;
      }
    });
});


})( jQuery );
