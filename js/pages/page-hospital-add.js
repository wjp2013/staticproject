
( function( $ ) {

var page = '#page-hospital-add';

$( function() {

    function adjust_specialbox_text() {
        $(page + ' .technology-box').each(function( index ) {
          var text_content = $( this ).text();
          var text_length = text_content.length;
          switch (text_length) {
              case 1:
              case 2:
              case 3:
              case 4:
              case 5:
                $( this ).css( "line-height", "40px" );
                break;
              case 6:
                text = text_content.substring(0, 3) + '<br>' + text_content.substring(3);
                $( this ).css( "line-height", "20px" );
                $( this ).html(text);
                break;
              case 7:
              case 8:
                text = text_content.substring(0, 4) + '<br>' + text_content.substring(4);
                $( this ).css( "line-height", "20px" );
                $( this ).html(text);
                break;
              case 9:
              case 10:
                text = text_content.substring(0, 5) + '<br>' + text_content.substring(5);
                $( this ).css( "line-height", "20px" );
                $( this ).html(text);
                break;
          }
        });
    }

    $(document).on('click', page + ' .btn-add-specialty', function() {
        var $btn = $(this);

        $('<div class="technology-box">&nbsp;</div>').appendTo('.phone-wrapper .technologies');

        var new_input = $btn.prev().clone().insertBefore($btn).focus();
        new_input.find('input').val('');
    })
    .on('click', page + ' .btn-reduce-specialty', function() {
        var $btn = $(this);
        var index = $btn.parent().parent().index();
        $btn.parent().parent().remove();
        $($('.technology-box').get(index)).remove();
    })
    .on('click', page + ' .btn-add-specialist', function() {
        var $btn = $(this);
        var $group = $btn.closest('.form-group').find('.inputs-specialist:last');
        var $newGroup = $group.clone()

        $('<div class="doctor">\
                <img alt="image" class="doctor-avatar" src="img/a4.jpg">\
                <div class="doctor-detail">\
                    <p><strong class="name"></strong> <span class="title"></small></p>\
                    <p class="specialties"></p>\
                </div>\
            </div>')
        .insertAfter($('.phone-wrapper .doctor:last'));

        $newGroup.find('input').val('');
        $newGroup.find('img').attr('src', 'img/a4.jpg');
        $newGroup.insertAfter($group)
            .find('input[type=text]:first')
            .focus();
    })
    .on('click', page + ' .btn-reduce-specialist', function() {
        var $btn = $(this);
        var index = $btn.parents('.inputs-specialist').index();
        $btn.parents('.inputs-specialist').remove();
        $($('.phone-wrapper .doctor').get(index)).remove();
    })

    .on('change', page + ' .input-image', function() {
        var input = this;
        var $input = $(this);
        var index = $input.closest('.inputs-specialist').index();

        if ( input.files && input.files[0] ) {
            var FR = new FileReader();
            FR.onload = function(e) {
                $input.prev().attr('src', e.target.result);
                $($('.doctor').get(index))
                    .find('.doctor-avatar')
                    .attr('src', e.target.result);
            };
            FR.readAsDataURL( input.files[0] );
        }
    })
    .on('click', page + ' .color-square', function() {
        $(this).addClass('selected')
            .siblings('.selected')
            .removeClass('selected');

        $('.phone-wrapper').find('.mobile-header, h3, .technology-box, .doctor .title, .mobile-footer')
            .attr('style', '');

        var index = $(this).index();
        $('.phone-wrapper').removeClass('color-0 color-1 color-2 color-custom');

        if ($(this).hasClass('color-square-custom')) {
            $('.phone-wrapper').addClass('color-custom');
        } else {
            $('.phone-wrapper').addClass('color-' + index);
        }
    })
    .on('click', page + ' .color-square-custom', function() {
        var color = $(this).css('background-color');
        var $phone = $('.phone-wrapper');

        $phone.find('.mobile-header, h3').css({ color: color });
        $phone.find('h3').css({ borderColor: color });
        $phone.find('.technology-box, .doctor .title, .mobile-footer').css({ backgroundColor: color });
    })
    .on('keyup', page + ' .input-hospital-name', function() {
        $('.hospital-name').text($(this).val());
    })
    .on('keyup', page + ' .input-technology', function() {
        var index = $(this).parent().parent().index();
        $($('.technology-box').get(index)).text($(this).val());
        adjust_specialbox_text();
    })
    .on('keyup', page + ' .input-name', function() {
        var index = $(this).closest('.inputs-specialist').index();
        $($('.doctor').get(index))
            .find('.name')
            .text($(this).val());
    })
    .on('keyup', page + ' .input-title', function() {
        var index = $(this).closest('.inputs-specialist').index();
        $($('.doctor').get(index))
            .find('.title')
            .text($(this).val());
    })
    .on('keyup', page + ' .input-specialties', function() {
        var index = $(this).closest('.inputs-specialist').index();
        $($('.doctor').get(index))
            .find('.specialties')
            .text($(this).val());
    })
    .on('keyup', page + ' .input-address', function() {
        $('.address').text($(this).val());
    });


    var $link = $(page + ' .link-add-color');
    $link.colorpicker().on('changeColor', function(e) {
        var $square = $('.color-square-custom');

        if (!$square.length) {
            $square = $('<span class="color-square color-square-custom"></span>');
        }

        $square.css({ backgroundColor: e.color.toHex() })
            .insertBefore($link);
        $square.click();
    });
});


})( jQuery );
